package com.example.home.murojaahbaru;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


public class Alkauayat1salah extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alkauayat1salah);

    }

    public void getSalah1(View view) {
        Intent i = new Intent(getApplicationContext(), AlkautsarAyat1.class);
        startActivity(i);
    }
}
