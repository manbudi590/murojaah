package com.example.home.murojaahbaru;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


public class Aliayat3salah extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aliayat3salah);
        }

    public void getSalah3(View view) {
        Intent i = new Intent(getApplicationContext(), AlikhlasAyat3.class);
        startActivity(i);
    }

}
