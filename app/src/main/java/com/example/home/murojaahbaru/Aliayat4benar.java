package com.example.home.murojaahbaru;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


public class Aliayat4benar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aliayat4benar);


    }

    public void getAyat4(View view) {
        Intent i = new Intent(getApplicationContext(), AlikhlasAyat4.class);
        startActivity(i);
    }

    public void getMainActivity(View view) {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
    }

    public void getExit(View view){
        finishAffinity();
    }

}




