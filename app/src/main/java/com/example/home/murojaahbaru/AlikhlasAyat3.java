package com.example.home.murojaahbaru;


import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;


public class AlikhlasAyat3 extends AppCompatActivity {
    private TextView txvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alikhlasayat3);
        txvResult = findViewById(R.id.txvResult);
        MediaPlayer player = MediaPlayer.create(this,R.raw.ayat3);
        player.start();

    }
    public void getSpeechInput (View view){
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent,10);
        } else {
            Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onActivityResult ( int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    txvResult.setText(result.get(0));
                    if (result.get(0).equals("lam yalid walam yulad")){
                        Toast.makeText(this, "Good", Toast.LENGTH_SHORT).show();
                        setContentView(R.layout.aliayat3benar);
                        MediaPlayer player = MediaPlayer.create(this, R.raw.benar);
                        MediaPlayer player1= MediaPlayer.create(this, R.raw.bacaan_benar);
                        player.start();
                        player1.start();

                    } else {
                        Toast.makeText(this, "Salah", Toast.LENGTH_SHORT).show();
                        setContentView(R.layout.aliayat3salah);
                        MediaPlayer player = MediaPlayer.create(this,R.raw.fail);
                        MediaPlayer player1 = MediaPlayer.create(this,R.raw.aliayat3salah);
                        player.start();
                        player1.start();


                    }

                }
                break;



        }

    }


    public void getAyat4(View view) {
        Intent i = new Intent(getApplicationContext(),AlikhlasAyat4.class);
        startActivity(i);
    }

    public void getSalah3(View view) {
        Intent i = new Intent(getApplicationContext(), AlikhlasAyat3.class);
        startActivity(i);
    }
}

