package com.example.home.murojaahbaru;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;


        public class MainActivity extends AppCompatActivity {
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_main);


                Button math= findViewById(R.id.coba);
                math.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), coba.class);
                        startActivity(i);
                    }
                });

                ImageButton math1= findViewById(R.id.math1);
                math1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), AlKautsar.class);
                        startActivity(i);
                    }
                });
                ImageButton math2= findViewById(R.id.math2);
                math2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), AlIkhlas.class);
                        startActivity(i);
                    }
                });
                ImageButton math3= findViewById(R.id.math3);
                math3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getApplicationContext(), AnNaas.class);
                        startActivity(i);
                    }
                });
                ImageButton math4= findViewById(R.id.math4);
                math4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finishAffinity();
                    }

                });

            }


            public void getExit(View view) {
                finishAffinity();
            }

        }


