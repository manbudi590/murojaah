package com.example.home.murojaahbaru;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


public class Aliayat4salah extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aliayat4salah);
    }

    public void getSalah4(View view) {
        Intent i = new Intent(getApplicationContext(), AlikhlasAyat4.class);
        startActivity(i);
    }

}
