package com.example.home.murojaahbaru;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


public class AnnAyat5salah extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.annayat5salah);
    }


    public void getSalah5(View view) {
        Intent i = new Intent(getApplicationContext(), AnnaasAyat5.class);
        startActivity(i);
    }
}
