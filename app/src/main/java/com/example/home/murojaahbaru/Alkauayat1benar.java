package com.example.home.murojaahbaru;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


public class Alkauayat1benar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alkauayat1benar);
    }

    public void getAyat2(View view) {
        Intent i = new Intent(getApplicationContext(), AlkautsarAyat2.class);
        startActivity(i);
    }
}
