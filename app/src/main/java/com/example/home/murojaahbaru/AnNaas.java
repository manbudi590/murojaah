package com.example.home.murojaahbaru;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

public class AnNaas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.annaas);
        MediaPlayer player= MediaPlayer.create(this, R.raw.surah_annas);
        player.start();


        ImageButton btn_play2 = findViewById(R.id.btn_play2);
        btn_play2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), AnnaasAyat1.class);
                startActivity(i);
            }
        });

    }
}
