package com.example.home.murojaahbaru;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class coba extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coba);
        MediaPlayer player= MediaPlayer.create(this, R.raw.surah_alkautsar);
        player.start();

        ImageButton btn_play1 = findViewById(R.id.btn_play2);
        btn_play1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), coba1.class);
                startActivity(i);
            }
        });
    }
}
