package com.example.home.murojaahbaru;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;


public class AnnAyat2benar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.annayat2benar);

        ImageButton btn_next_2 = findViewById(R.id.btn_next_2);
        btn_next_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),AnnaasAyat3.class);
                startActivity(i);
            }
        });
    }

}
