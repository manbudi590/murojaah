package com.example.home.murojaahbaru.Api;

import com.example.home.murojaahbaru.model.ResObj;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface BaseApiService {

    @GET("surat/{nomor}.json")
    Call<ResObj> login(@Path("nomor") String nomor);

}
